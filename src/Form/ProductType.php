<?php

namespace App\Form;

use App\Entity\Product;
use App\Form\ProductImageType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('product_images', CollectionType::class, [
                'entry_type' => ProductImageType::class,
                'required' => true,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'label' => 'Fichier(s) :',
                'by_reference' => false
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
